//
//  FOTMainViewController.h
//  FBLogin
//
//  Created by Ricardo Guillen on 10/1/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FOTMainViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end
