//
//  FOTLoginViewController.m
//  FBLogin
//
//  Created by Ricardo Guillen on 10/1/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import "FOTLoginViewController.h"
#import "FOTAppDelegate.h"

@interface FOTLoginViewController ()

@end

@implementation FOTLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.loginActivityIndicator setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)performFbLogin:(id)sender {
    [self.loginActivityIndicator setHidden:NO];
    [self.loginActivityIndicator startAnimating];
    
    FOTAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate openSession];
}

- (void)loginFailed
{
    // User switched back to the app without authorizing. Stay here, but
    // stop the spinner.
    [self.loginActivityIndicator setHidden:YES];
    [self.loginActivityIndicator stopAnimating];
}


@end
